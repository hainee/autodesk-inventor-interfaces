﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace AutodeskInventorInterfaces.Model
{
  [JsonObject]
  public class SetParametersParams
  {
    public string Template { get; set; }

    public List<ClientDocumentParameter> Documents { get; set; }

    /// <summary>
    /// 导出文件的类型定义
    /// </summary>
    // public ExportFileTypes Exports { get; set; } = new ExportFileTypes();
  }

  public class ClientDocumentParameter
  {
    public string Template { get; set; }

    public List<ClientParameter> Parameters { get; set; }
  }

  /// <summary>
  /// 导出PDF格式的配置参数
  /// </summary>
  public class ExportPDFFileParams
  {
    public bool Enable { get; set; }

    public string PageSize { get; set; } = "A4";

    public string BackgroundColor { get; set; } = "black";

    /// <summary>
    /// 
    /// </summary>
    public Rectangle Margin { get; set; } = new Rectangle(0, 0, 0, 0);
  }

}