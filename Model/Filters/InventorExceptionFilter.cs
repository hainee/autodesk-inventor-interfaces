﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutodeskInventorInterfaces.Model.Exceptions;

namespace AutodeskInventorInterfaces.Model.Filters
{
  /// <summary>
  /// 错误处理Filter
  /// </summary>
  public class InventorExceptionFilter : IExceptionFilter
  {
    public void OnException(ExceptionContext filterContext)
    {
      var ex = filterContext.Exception as InventorException;
      if (ex == null)
      {
        ex = InventorException.未知错误.Create($"发生在：{filterContext.Controller.GetType().Name}").SetDetail(filterContext.Exception.Message);
      }
      filterContext.Result = new JsonResult
      {
        Data = new
        {
          code = ex.ErrorCode,
          message = ex.Message,
          detail = ex.Detail
        },
        JsonRequestBehavior = JsonRequestBehavior.AllowGet
      };
      filterContext.ExceptionHandled = true;
    }
  }
}