﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace AutodeskInventorInterfaces.Model
{
  [JsonObject]
  public class ExportParams
  {
    public string File { get; set; }

    public ExportFileTypes Exports { get; set; }
  }
}