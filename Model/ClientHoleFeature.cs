﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutodeskInventorInterfaces.Extend;
using Inventor;
using Newtonsoft.Json;

namespace AutodeskInventorInterfaces.Model
{
  /// <summary>
  /// 孔螺纹特征客户端数据模型
  /// </summary>
  [JsonObject]

  public class ClientHoleFeature
  {
    public string Name { get; set; }

    public ClientTapInfo TapInfo { get; set; }

    public ClientParameter BottomTipAngle { get; set; }

    public ClientHoleFeature() { }

    public ClientHoleFeature(HoleFeature holeFeature)
    {
      this.Name = holeFeature.Name;
      if (holeFeature.TapInfo != null)
      {
        this.TapInfo = new ClientTapInfo(holeFeature.TapInfo as HoleTapInfo);
      }

      if (holeFeature.BottomTipAngle != null)
      {
        this.BottomTipAngle = holeFeature.BottomTipAngle.ToClientParameter();
      }
    }
  }
}