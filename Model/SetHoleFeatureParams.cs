﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace AutodeskInventorInterfaces.Model
{
  /// <summary>
  /// 客户端回传设置孔特征参数的模型
  /// </summary>
  [JsonObject]
  public class SetHoleFeatureParams
  {
    /// <summary>
    /// 根模型文档
    /// </summary>
    public string Template { get; set; }

    /// <summary>
    /// 要设置的关联文档的孔特征参数集合
    /// </summary>
    public List<ClientDocumentHoleFeatures> HoleFeatures { get; set; }
  }
}