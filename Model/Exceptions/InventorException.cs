﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutodeskInventorInterfaces.Model.Exceptions
{
  /// <summary>
  /// 错误信息基类
  /// </summary>
  public class InventorException : Exception
  {

    public static InventorException 参数错误 = new InventorException(10000, "参数错误！");

    /// <summary>
    /// 指定的零部件文档不存在
    /// </summary>
    public static InventorException 文档未找到 = new InventorException(10001, "指定的零部件文档不存在！");

    /// <summary>
    /// 文档打开失败
    /// </summary>
    public static InventorException 文档打开失败 = new InventorException(10002, "打开指定的零部件文档：'{0}' 失败！");

    /// <summary>
    /// 文档克隆失败
    /// </summary>
    public static InventorException 文档克隆失败 = new InventorException(10003, "克隆指定的零部件文档：'{0}' 失败！");

    /// <summary>
    /// 文档导出隆失败
    /// </summary>
    public static InventorException 文档导出失败 = new InventorException(10004, "导出指定的零部件文档：'{0}' 失败！");

    /// <summary>
    /// 文档关闭隆失败
    /// </summary>
    public static InventorException 文档关闭失败 = new InventorException(10005, "关闭指定的零部件文档：'{0}' 失败！");

    /// <summary>
    /// 模板文档移除失败
    /// </summary>
    public static InventorException 模板文档移除失败 = new InventorException(10006, "移除指定的零部件文档：'{0}' 失败！");




    /// <summary>
    /// 文档参数设置失败
    /// </summary>
    public static InventorException 参数设置失败 = new InventorException(10010, "文档：'{0}'的参数设置失败！");

    /// <summary>
    /// 模板零部件参数无效
    /// </summary>
    public static InventorException 模板零部件参数无效 = new InventorException(10011, "模板零部件的文件名参数无效！");

    /// <summary>
    /// 孔特征设置失败
    /// </summary>
    public static InventorException 孔特征设置失败 = new InventorException(10012, "孔特征设置失败！");




    /// <summary>
    /// 指定的转换格式无效
    /// </summary>
    public static InventorException 指定的转换格式无效 = new InventorException(10020, "指定的转换格式'{0}'无效！");

    /// <summary>
    /// 格式转换失败
    /// </summary>
    public static InventorException 文档格式转换失败 = new InventorException(10021, "{0}格式转换失败！");

    /// <summary>
    /// 文档类型错误
    /// </summary>
    public static InventorException 文档类型错误 = new InventorException(10022, "文档类型错误，期望'{0}'类型！");

    /// <summary>
    /// 指定的对象未找到
    /// </summary>
    public static InventorException 对象未找到 = new InventorException(10030, "对象'{0}'未找到！");


    /// <summary>
    /// 未知错误
    /// </summary>
    public static InventorException 未知错误 = new InventorException(11000, "发生未知错误'{0}'。");  

    public InventorException(int errorCode, string message = null, string detail = null)
    {
      this.ErrorCode = errorCode;
      this.Message = message;
      this.Detail = detail;
    }

    public int ErrorCode { get; set; }

    public new string Message { get; set; }

    public string Detail { get; set; }

    /// <summary>
    /// 克隆当前错误信息对象
    /// </summary>
    /// <param name="args">格式化错误消息的参数集合</param>
    /// <returns></returns>
    public InventorException Create(params object[] args)
    {
      var message = string.Format(this.Message, args);
      return new InventorException(this.ErrorCode, message, this.Detail);
    }

    public InventorException SetDetail(string detail)
    {
      this.Detail = detail;
      return this;
    }
  }
}