﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace AutodeskInventorInterfaces.Model
{
  public class PrintToFileParams
  {
    /// <summary>
    /// 默认打印机名称
    /// </summary>
    public static string DefaultPrinter = ConfigurationManager.AppSettings["DefaultPDFPrinter"];

    /// <summary>
    /// 默认后缀
    /// </summary>
    public static string DefaultFileExtension = ".pdf";

    /// <summary>
    /// 零部件文件
    /// </summary>
    public string PartFile { get; set; }

    /// <summary>
    /// 打印机名称
    /// </summary>
    public string Printer { get; set; }

    /// <summary>
    /// 文件名
    /// </summary>
    public string FileName { get; set; }
  }
}