﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Inventor;
using Newtonsoft.Json;

namespace AutodeskInventorInterfaces.Model
{
  [JsonObject]
  public class ClientParameter
  {
    public string Name { get; set; }

    public double Value { get; set; }

    public string Units { get; set; }
  }
}