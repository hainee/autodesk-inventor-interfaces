﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutodeskInventorInterfaces.Model
{

  /// <summary>
  /// 文件导出配置参数
  /// </summary>
  public class ExportFileTypes
  {
    public bool Sketch { get; set; }

    public bool DWG { get; set; }

    public bool STL { get; set; }
  }
}