﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace AutodeskInventorInterfaces.Model
{
  [JsonObject]
  public class UploadFileData
  {
    public string FileName { get; set; }

    public string Data { get; set; }
  }
}