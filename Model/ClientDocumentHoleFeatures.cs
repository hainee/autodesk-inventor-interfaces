﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Newtonsoft.Json;

namespace AutodeskInventorInterfaces.Model
{
  /// <summary>
  /// 客户端传输孔特征参数的模型
  /// </summary>
  [JsonObject]
  public class ClientDocumentHoleFeatures
  {
    public string Template { get; set; }

    public List<ClientHoleFeature> HoleFeatures { get; set; }
  }
}
