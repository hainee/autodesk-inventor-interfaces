﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using AutodeskInventorInterfaces.Extend;
using AutodeskInventorInterfaces.Service;
using Inventor;

namespace AutodeskInventorInterfaces.Model.Translators
{
  public class InventorTranslatorDWG : InventorTranslator
  {
    public InventorTranslatorDWG() : base()
    {
      Name = "转换器: DWG";
      ClassId = "{C24E3AC2-122E-11D5-8E91-0010B541CD80}";
      Ext = "dwg";
    }


    public override object Translate(dynamic doc, string fileName, ExportFileTypes exportFileTypes)
    {
      var dwgFile = CreateFileName(fileName);
      doc.Activate();
      InventorApplication.TranslateTo(doc, this, dwgFile); // 执行格式转换
      return dwgFile.Replace($".{Ext}", ".zip").ToRelativePath();
    }
  }
}