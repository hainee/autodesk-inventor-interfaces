﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AutodeskInventorInterfaces.Model.Translators
{
  public class InventorTranslatorDWF : InventorTranslator
  {
    public InventorTranslatorDWF() : base()
    {
      Name = "转换器: DWF";
      ClassId = "{0AC6FD95-2F4D-42CE-8BE0-8AEA580399E4}";
      Ext = "dwf";
    }
  }
}