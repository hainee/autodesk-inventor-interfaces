﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutodeskInventorInterfaces.Extend;
using Inventor;

namespace AutodeskInventorInterfaces.Model.Translators
{
  public class InventorTranslator
  {
    public static InventorTranslator TranslatorDWG = new InventorTranslatorDWG();

    public static InventorTranslator TranslatorSketch = new InventorTranslatorSketch();

    public static InventorTranslator TranslatorDWF = new InventorTranslatorDWF();

    public static InventorTranslator TranslatorSTL = new InventorTranslatorSTL();


    public static IDictionary<string, InventorTranslator> Translators = new Dictionary<string, InventorTranslator>()
    {
      {
        "DWG", TranslatorDWG
      },
      {
        "DWF", TranslatorDWF
      },
      {
        "SKETCH", TranslatorSketch
      },
      {
        "STL", TranslatorSTL
      },
    };


    public string Name { get; set; }

    public string ClassId { get; set; }

    public string Ext { get; set; }

    /// <summary>
    /// 
    /// </summary>
    public virtual InventorTranslator SetOptions(NameValueMap options)
    {
      return this;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="partDoc"></param>
    /// <param name="fileName"></param>
    /// <returns></returns>
    public virtual object Translate(dynamic partDoc, string fileName, ExportFileTypes exportFileTypes)
    {
      var resultFile = this.CreateFileName(fileName);
      partDoc.SaveAs(resultFile, true);
      return resultFile.ToRelativePath();
    }

    public virtual string CreateFileName(string fileName)
    {
      return $"{fileName}.{Ext}";
    }
  }
}