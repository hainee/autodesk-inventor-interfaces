﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web;
using AutodeskInventorInterfaces.Extend;
using AutodeskInventorInterfaces.Model.Exceptions;
using AutodeskInventorInterfaces.Service;
using Inventor;

namespace AutodeskInventorInterfaces.Model.Translators
{
  /// <summary>
  /// PDF格式转换器
  /// </summary>
  public class InventorTranslatorPDF : InventorTranslator
  {
    public InventorTranslatorPDF() : base()
    {
      ClassId = "{0AC6FD96-2F4D-42CE-8BE0-8AEA580399E4}";
      Name = "转换器: PDF";
      Ext = "pdf";
    }

    public override InventorTranslator SetOptions(NameValueMap options)
    {
      options.Value["All_Color_AS_Black"] = 1;
      options.Value["Sheet_Range"] = PrintRangeEnum.kPrintAllSheets;
      return this;
    }

    public override object Translate(dynamic partDoc, string fileName, ExportFileTypes exportFileTypes)
    {
      var pdfFile = CreateFileName(fileName);
      partDoc.Activate();
      var mainView = partDoc.Views[1];
      if (mainView != null)
      {
        mainView.WindowState = WindowsSizeEnum.kMaximize; //必须将主视图窗口最大化
        Thread.Sleep(2000); // 延时2秒确保窗口已最大化
        InventorApplication.TranslateTo(partDoc, this, pdfFile); // 执行格式转换
        return pdfFile.ToRelativePath();
      }
      else
      {
        throw InventorException.文档格式转换失败.Create(Name).SetDetail("零部件主视图不存在！");
      }
    }
  }
}