﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutodeskInventorInterfaces.Extend;
using AutodeskInventorInterfaces.Service;

namespace AutodeskInventorInterfaces.Model.Translators
{
  public class InventorTranslatorSTL : InventorTranslator
  {
    public InventorTranslatorSTL() : base()
    {
      Name = "转换器: STL Export";
      ClassId = "{533E9A98-FC3B-11D4-8E7E-0010B541CD80}";
      Ext = "stl";
    }

    public override object Translate(dynamic doc, string fileName, ExportFileTypes exportFileTypes)
    {
      var stlFile = CreateFileName(fileName);
      doc.Activate();
      InventorApplication.TranslateTo(doc, this, stlFile); // 执行格式转换
      return stlFile.ToRelativePath();
    }
  }
}