﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using AutodeskInventorInterfaces.Extend;
using Inventor;
using Newtonsoft.Json;

namespace AutodeskInventorInterfaces.Model
{
  [JsonObject]

  public class ClientTapInfo
  {
    /// <summary>
    /// 螺纹类型
    /// </summary>
    public string ThreadType { get; set; }

    /// <summary>
    /// 指示螺纹是公制螺纹还是英制螺纹的属性
    /// </summary>
    public bool Metric { get; set; }

    /// <summary>
    /// 是否全螺纹
    /// </summary>
    public bool Internal { get; set; }

    /// <summary>
    /// 是否右旋
    /// </summary>
    public bool RightHanded { get; set; }

    /// <summary>
    /// 螺纹规格
    /// </summary>
    public string ThreadDesignation { get; set; }

    //public string CustomThreadDesignation { get; set; }

    /// <summary>
    /// 获取此螺纹是圆柱体还是圆锥体的全长（全螺纹）
    /// </summary>
    public bool? FullThreadDepth { get; set; }

    /// <summary>
    /// 螺纹类别
    /// </summary>
    public string Class { get; set; }

    /// <summary>
    /// 螺纹尺寸最大值
    /// </summary>
    public double? MajorDiameterMax { get; set; }

    /// <summary>
    /// 螺纹尺寸最小值
    /// </summary>
    public double? MajorDiameterMin { get; set; }

    public double? MinorDiameterMax { get; set; }

    public double? MinorDiameterMin { get; set; }

    public double? PitchDiameterMax { get; set; }

    public double? PitchDiameterMin { get; set; }

    public double? TapDrillDiameter { get; set; }

    //public string NominalSize { get; set; }

    //public double Pitch { get; set; }

    public ClientParameter ThreadDepth { get; set; }

    public ClientTapInfo() { }

    public ClientTapInfo(HoleTapInfo tapInfo)
    {
      ThreadType = tapInfo.ThreadType;
      Metric = tapInfo.Metric;
      Internal = tapInfo.Internal;
      RightHanded = tapInfo.RightHanded;
      ThreadDesignation = tapInfo.ThreadDesignation;
      FullThreadDepth = tapInfo.FullThreadDepth;
      Class = tapInfo.Class;
      MajorDiameterMax = (double)tapInfo.MajorDiameterMax;
      MajorDiameterMin = (double)tapInfo.MajorDiameterMin;
      MinorDiameterMax = (double)tapInfo.MinorDiameterMax;
      MinorDiameterMin = (double)tapInfo.MinorDiameterMin;
      PitchDiameterMax = (double)tapInfo.PitchDiameterMax;
      PitchDiameterMin = (double)tapInfo.PitchDiameterMin;
      TapDrillDiameter = tapInfo.TapDrillDiameter;
      //try
      //{
      //  ThreadDepth = tapInfo.ThreadDepth.ToClientParameter();
      //} catch {}
    }
  }
}