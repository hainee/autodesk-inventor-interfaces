﻿using System.Web;
using System.Web.Mvc;
using AutodeskInventorInterfaces.Model.Filters;

namespace AutodeskInventorInterfaces
{
  public class FilterConfig
  {
    public static void RegisterGlobalFilters(GlobalFilterCollection filters)
    {
      filters.Add(new InventorExceptionFilter());
      filters.Add(new JsonResultFilter());
    }
  }
}
