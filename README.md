# AutodeskInventorInterfaces

#### 介绍
一个基于Autodesk Inventor2014的可参数化修改零部件(.ipt)并可导出为其它格式的API服务

#### 软件架构
Windows + ASP.Net Mvc5 Web API

#### 错误描述
    Code		描述

1. 0			执行成功
1. 10000		参数错误
1. 10001		指定的零部件文档不存在！
1. 10002		打开指定的零部件文档失败！
1. 10003		克隆指定的零部件文档失败！
1. 10004		导出指定的零部件文档失败！
1. 10005		关闭指定的零部件文档失败！
1. 10006		移除指定的零部件文档失败！
1. 10010		文档的参数设置失败！
1. 10011		模板零部件的文件名参数无效！
1. 10020		指定的转换格式无效！
1. 10021		格式转换失败！
1. 11000		发生未知错误。

  
#### 接口说明
返回的数据结构：  
{  
　　"code": 0,			// 错误编码，0：成功  
　　"data": null,		// 返回的数据对象  
　　"message": "",		// 返回的错误消息  
　　"detail": ""			// 错误详情  
}  
1. [GET] home/getPartFiles  
获取零部件模板集合  
1. [GET] home/getParameters?partFile=***  
获取指定零部件模板的参数列表，返回data的数据结构：  
[{  
　　"name": "厚度",  
　　"value": 0.05,  
　　"units": ""  
}]  
1. [POST] home/setParameters  
设置指定零部件文档的参数并生成DWG格式的文件，同时另存零部件文件为副本。  
请求的JSON数据结构：  
{  
　　"template": "/parts/test01.ipt",  
　　"parameters": [  
　　　　{  
　　　　　　"name": "d0",  
　　　　　　"value": 20  
　　　　}  
　　]  
}  
返回data的JSON数据结构：  
{  
　　"partFile": "零部件文件的相对路径",  
　　"dwgFile": "导出的DWG格式文件的相对路径"  
}  
然后在浏览器中直接下载文件即可。  
1. [POST] home/uploadTemplatePartFile  
A.上传零部件模板，采用标准的POST方式上传文件，成功返回data:["上传成功的文件相对路径"]  
B.上传零部件模板，采用POST JSON数据方式上传文件，请求的JSON数据结构：  
[{  
　　"fileName": "test01.ipt",  
　　"data": "Base64编码的文件数据"  
},  
{  
　　"fileName": "test02.ipt",  
　　"data": "Base64编码的文件数据"  
}]  
返回data的JSON数据结构：  
[  
　　"第一个文件的服务器相对路径",  
　　"第二个文件的服务器相对路径",  
]  
1. [DELETE] home/removeTemplatePartFile?partFile=***  
删除指定的零部件模板，成功返回data:true  